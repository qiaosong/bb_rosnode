
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>
#include <iostream>
#include <cstring>
#include <string>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#define BUFLEN 512
#define PORT 9930


using namespace std;

int globalcount =0;

int globalcontrolflag =0;  // 0--stop recording   1--start recording 2--create new directory
string globaldirectory;
string log_directory = "";


void chatterCallback(const std_msgs::String::ConstPtr& str_msg)
{

  printf("I heard: [%s] [%d]", str_msg->data.c_str(),globalcount); fflush(stdout);
  return;


  string  s;

  if(str_msg) {
       s = str_msg->data;

    if (!s.size()) {

      printf("STOP LOGGING received\n"); 
      globalcontrolflag=0;

    }

    // START LOGGING message

    else {

      printf("START LOGGING received\n"); 
      globalcontrolflag=1;

      if (log_directory.size()) {

    printf("IS logging currently to %s\n", log_directory.c_str());
    }

    if (!s.compare(log_directory)) {
      printf("SAME directory...no change\n"); 
      return;
    }
    else {
      printf("NEW directory...stopping old one\n"); 
      globalcontrolflag=2;
      globaldirectory=str_msg->data;
         }

     }//else
        }//if(str_msg)


    log_directory = s;


}


void err(char *s)
{
    perror(s);
    exit(1);
}



int main(int argc, char **argv)
{
    ros::init(argc, argv, "BBSubscriber");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("drc_drive_logging_signal", 10, chatterCallback);

    struct sockaddr_in serv_addr;
    int sockfd, i, slen=sizeof(serv_addr);
    char buf[BUFLEN];
 
    //Initialize UDP
    char *address = "127.0.0.1";

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1)
        err("socket");
 
    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    if (inet_aton(address, &serv_addr.sin_addr)==0)
    {
        fprintf(stderr, "inet_aton() failed\n");
        exit(1);
    }



  while(ros::ok())
  {

   if(globalcontrolflag != 0)
   {
   sprintf(buf,globaldirectory.c_str());
     
   //globalcontrolflag = 1;
   }
   else
   {
   globalcontrolflag = 0;
   sprintf(buf, "%d", globalcontrolflag); 
   }
  
   //Send UDP packet
   sendto(sockfd, buf, BUFLEN, 0, (struct sockaddr*)&serv_addr, slen);

   ros::spinOnce();
   //sleep(10);
  }

  close(sockfd);
  return 0;
}


