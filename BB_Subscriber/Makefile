
#####################################################################
# Copyright (c) 2008 Point Grey Research Inc.
#
# This Makefile is free software; Point Grey Research Inc.
# gives unlimited permission to copy and/or distribute it,
# with or without modifications, as long as this notice is preserved.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY, to the extent permitted by law; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.
#
#####################################################################

ROOT_INCLUDE = /usr/local/include
USER_INCLUDE = /usr/include
ROS_INCLUDE = /opt/ros/fuerte/include
ROOT_LIB = /usr/local/lib
USER_LIB = /usr/local/include/triclops/lib
LOCALLIB = ../pgrlibdcstereo
PCL_LIB = /opt/ros/fuerte/stacks/perception_pcl_fuerte_unstable/pcl16/lib
ROSPCL_INCLUDE =/opt/ros/fuerte/stacks/perception_pcl_fuerte_unstable/pcl16/include/pcl-1.6
ROSPCL2_INCLUDE =/opt/ros/fuerte/stacks/perception_pcl_fuerte_unstable/pcl16/msg_gen/cpp/include
# compilation flags
CPPFLAGS += -I.

# libdc1394 installed in /usr/local/include location
CPPFLAGS += -I$(ROOT_INCLUDE)/dc1394
CPPFLAGS += -I$(ROSPCL_INCLUDE)/
CPPFLAGS += -I$(ROSPCL2_INCLUDE)/ 
CPPFLAGS += -I$(USER_INCLUDE)/eigen3
CPPFLAGS += -I$(USER_INCLUDE)/vtk-5.8
CPPFLAGS += -I$(USER_INCLUDE)/boost
CPPFLAGS += -I$(ROS_INCLUDE)
CPPFLAGS += -I$(LOCALLIB)
CPPFLAGS += -I$(PCL_LIB)
CPPFLAGS += -I$(ROOT_INCLUDE)/triclops/include
CPPFLAGS += -Wall -g
CPPFLAGS += -DLINUX

#CPPFLAGS += -Wall -O3

LDFLAGS	+= -L. -L$(ROOT_LIB)/triclops
LDFLAGS += -L$(LOCALLIB)
LDFLAGS += -L$(ROOT_LIB)
LDFLAGS += -L$(USER_LIB)
LDFLAGS += -L$(PCL_LIB)
LIBS    += -ldc1394 -lraw1394 -pthread
LIBS	+= -lpgrlibdcstereo -ltriclops -lpnmutils
LIBS    += -L/usr/lib -lpthread -ldl -lm -std=gnu++0x -std=c++0x -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_video -lopencv_objdetect 


LIBS    += -L/usr/local/lib -lpcl16pcl_visualization -lpcl16pcl_common -lpcl16pcl_io -lpcl16pcl_kdtree -lpcl16pcl_io_ply -lpcl16pcl_segmentation

LIBS    += -L/opt/ros/fuerte/lib -lrosconsole -lrostime -lroscpp_serialization -ltopic_tools -lmessage_filters -lrospack -lroscpp -lxmlrpcpp
   
LIBS    += -L/usr/lib -lvtkWidgets -lvtkHybrid -lvtkRendering -lvtkGraphics -lvtkverdict -lvtkImaging -lvtkIO -lvtkFiltering -lvtkCommon -lvtkDICOMParser -lvtkmetaio -lvtksys -lvtkftgl -lvtkexoIIc 

LIBS    += -L/usr/lib -lboost_system -lboost_system-mt -lboost_filesystem -lboost_date_time

CFLAGS += `pkg-config --cflags opencv`
LIBS += `pkg-config --libs opencv`
# executable name and contents
EXEC1		= BBSubscriber
EXEC1SRC	= $(EXEC1).cpp

EXECS		= $(EXEC1) 


all:	bin

bin: $(EXECS)

$(EXEC1): $(EXEC1SRC:%.cpp=%.o)
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBS)



%.o:%.cpp
	g++ -c $(CXXFLAGS) $(CPPFLAGS) $*.cpp -o $*.o

clean:
	rm -f *~ *.o *.d $(EXECS) *.pgm *.ppm

#####################################################################
#
# $Id: Makefile,v 1.2 2008/12/11 00:00:21 warrenm Exp $
# $Author: warrenm $
# $Revision: 1.2 $
# $Date: 2008/12/11 00:00:21 $
#
#####################################################################

