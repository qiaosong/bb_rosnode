/**************************************************************************
 *
 * Copyright:	(C) 2006,2007,2008 Don Murray donm@ptgrey.com
 *
 * Description:
 *
 *    Get an image set from a Bumblebee or Bumblebee2 via DMA transfer
 *    using libdc1394 and process it with the Triclops stereo
 *    library. Based loosely on 'grab_gray_image' from libdc1394 examples.
 *
 *-------------------------------------------------------------------------
 *     License: LGPL
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *************************************************************************/

//=============================================================================
// Copyright © 2006,2007,2008 Point Grey Research, Inc. All Rights Reserved.
//
// This software is the confidential and proprietary information of Point
// Grey Research, Inc. ("Confidential Information").  You shall not
// disclose such Confidential Information and shall use it only in
// accordance with the terms of the license agreement you entered into
// with Point Grey Research Inc.
//
// PGR MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
// SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE, OR NON-INFRINGEMENT. PGR SHALL NOT BE LIABLE FOR ANY DAMAGES
// SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
// THIS SOFTWARE OR ITS DERIVATIVES.
//
//=============================================================================

//=============================================================================
//
// BBSubscriber.cpp
//
// Modified by Qiaosong Wang
//=============================================================================



//=============================================================================
// System Includes
//=============================================================================
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>

#include <dc1394/conversions.h>
#include <dc1394/control.h>
#include <dc1394/utils.h>

#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h>

//=============================================================================
// opencv Includes
//=========================================================================
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv/cxcore.h>

using namespace cv;
using namespace std;


//=============================================================================
// PGR Includes
//=============================================================================
#include "pgr_registers.h"
#include "pgr_stereocam.h"

//=============================================================================
// ROS Includes
//=============================================================================

#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>
#include <iostream>
#include <cstring>
#include <string>
//=============================================================================
// UDP Includes
//=============================================================================

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#define BUFLEN 512
#define PORT 9930


//Global variables
string log_directory = "";
int globalcontrolflag =0;  // 0--stop recording   1--start recording 2--create new directory
string globaldirectory;



//Clean 1394 Camera
void
cleanup_and_exit( dc1394camera_t* camera )
{
   dc1394_capture_stop( camera );
   dc1394_video_set_transmission( camera, DC1394_OFF );
   dc1394_camera_free( camera );
   exit( 0 );
}


//For directory name
string UD_datetime_string()
{
  time_t ltime;
  struct tm *today;
  char *s;

  s = (char *) calloc(30, sizeof(char));

  time(&ltime);
  today = localtime(&ltime);

  strftime(s, 30, "%Y_%b_%d_%a_%I_%M_%S_%p", today);

  return s;
}


//UDP error handling
void udperr(char *str)
{
    perror(str);
    exit(1);
}



int main(int argc, char **argv)
{



   //Initialize ROS
  ros::init(argc, argv, "BBSubscriber");

 // THis doesn't work with Triclops
 // ros::NodeHandle BBnode;
 // ros::Subscriber sub = BBnode.subscribe("drc_drive_logging_signal", 10, chatterCallback);


   //Initialize BB
   dc1394camera_t* 	camera;
   dc1394error_t 	err;
   dc1394_t * d;
   dc1394camera_list_t * list;
   unsigned int nThisCam;

   // Find cameras on the 1394 buses
   d = dc1394_new ();

   // Enumerate cameras connected to the PC
   err = dc1394_camera_enumerate (d, &list);
   if ( err != DC1394_SUCCESS )
   {
       fprintf( stderr, "Unable to look for cameras\n\n"
             "Please check \n"
	         "  - if the kernel modules `ieee1394',`raw1394' and `ohci1394' "
	         "are loaded \n"
	         "  - if you have read/write access to /dev/raw1394\n\n");
       return 1;
    }

    if (list->num == 0)
    {
        fprintf( stderr, "No cameras found!\n");
        return 1;
    }

    printf( "There were %d camera(s) found attached to your PC\n", list->num  );

    // Identify cameras. Use the first stereo camera that is found
    for ( nThisCam = 0; nThisCam < list->num; nThisCam++ )
    {
        camera = dc1394_camera_new(d, list->ids[nThisCam].guid);

        if(!camera)
        {
            printf("Failed to initialize camera with guid %llx", list->ids[nThisCam].guid);
            continue;
        }

        printf( "Camera %d model = '%s'\n", nThisCam, camera->model );

        if ( isStereoCamera(camera))
        {
            printf( "Using this camera\n" );
            break;
        }
        dc1394_camera_free(camera);
   }

   if ( nThisCam == list->num )
   {
      printf( "No stereo cameras were detected\n" );
      return 0;
   }

   // Free memory used by the camera list
   dc1394_camera_free_list (list);

   PGRStereoCamera_t stereoCamera;

   // query information about this stereo camera
   err = queryStereoCamera( camera, &stereoCamera );
   if ( err != DC1394_SUCCESS )
   {
      fprintf( stderr, "Cannot query all information from camera\n" );
      cleanup_and_exit( camera );
   }

   if ( stereoCamera.nBytesPerPixel != 2 )
   {
      // can't handle XB3 3 bytes per pixel
      fprintf( stderr,
	       "Example has not been updated to work with XB3 in 3 camera mode yet!\n" );
      cleanup_and_exit( stereoCamera.camera );
   }

   // set the capture mode
   printf( "Setting stereo video capture mode\n" );
   err = setStereoVideoCapture( &stereoCamera );
   if ( err != DC1394_SUCCESS )
   {
      fprintf( stderr, "Could not set up video capture mode\n" );
      cleanup_and_exit( stereoCamera.camera );
   }

   // have the camera start sending us data
   printf( "Start transmission\n" );
   err = startTransmission( &stereoCamera );
   if ( err != DC1394_SUCCESS )
   {
      fprintf( stderr, "Unable to start camera iso transmission\n" );
      cleanup_and_exit( stereoCamera.camera );
   }

   // give the auto-gain algorithms a chance to catch up
   printf( "Auto-stablizing, will start in 5 secs\n" );
   sleep( 5 );
 

   // Allocate all the buffers.
   // Unfortunately color processing is a bit inefficient because of the number of
   // data copies.  Color data needs to be
   // - de-interleaved into separate bayer tile images
   // - color processed into RGB images
   // - de-interleaved to extract the green channel for stereo (or other mono conversion)

   // size of buffer for all images at mono8
   unsigned int   nBufferSize = stereoCamera.nRows *
                                stereoCamera.nCols *
                                stereoCamera.nBytesPerPixel;
   // allocate a buffer to hold the de-interleaved images
   unsigned char* pucDeInterlacedBuffer = new unsigned char[ nBufferSize ];
   unsigned char* pucRGBBuffer = NULL;
   unsigned char* pucGreenBuffer = NULL;

   TriclopsInput input;

   int count=0;
   pucRGBBuffer 	= new unsigned char[ 3 * nBufferSize ];
   pucGreenBuffer 	= new unsigned char[ nBufferSize ];

   unsigned char* pucRightRGB	= NULL;
   unsigned char* pucLeftRGB	= NULL;
   unsigned char* pucCenterRGB	= NULL;
   TriclopsError e;
   TriclopsContext triclops;

   printf( "Getting TriclopsContext from camera (slowly)... \n" );
   e = getTriclopsContextFromCamera( &stereoCamera, &triclops );
   if ( e != TriclopsErrorOk )
   {
      fprintf( stderr, "Can't get context from camera\n" );
      cleanup_and_exit( camera );
      return 1;
   }
   printf( "...done\n" );

    //Finish initializing BB



   //Names for logging images
   char ssl[20],ssr[20],ssrl[20],ssrr[20],ssd[20],temp[20];

   char currenttime[30];
   struct timeval now;
   //Timestamp
   ofstream outputFile;
   const char * Data_saving_path;

   //Default Saving path
   //std::stringstream ss;
   //ss << ros::Time::now()<<endl;
    
   //Default saving location
   /*
   globaldirectory=UD_datetime_string().c_str();
   Data_saving_path = UD_datetime_string().c_str();

   mkdir(Data_saving_path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); 
   chdir(Data_saving_path);
   mkdir("raw", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); 
   mkdir("rect", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); 
   mkdir("disparity", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
   outputFile.open("timestamp.txt");
   */


    //Initializing UDP
    struct sockaddr_in my_addr, cli_addr;
    int sockfd, i;
    socklen_t slen=sizeof(cli_addr);
    char buf[BUFLEN];
 
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1)
      udperr("socket");
    else
      printf("Server : Socket() successful\n");
 
    bzero(&my_addr, sizeof(my_addr));
    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(PORT);
    my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
 
    if (bind(sockfd, (struct sockaddr* ) &my_addr, sizeof(my_addr))==-1)
      udperr("bind");
    else
      printf("Server : bind() successful\n");





  /////////////////////////Main Loop Begins Here///////////////////////////

  while(1) 
  
  {
  
    //Update ROS control information

    if (recvfrom(sockfd, buf, BUFLEN, 0, (struct sockaddr*)&cli_addr, &slen)==-1)
    udperr("recvfrom()");

    printf("Received packet from %s:%d\nData: %s\n\n",
    inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port), buf);

 
    if(strcmp(buf,"0") == 0)
    globalcontrolflag = 0;

    else
    {
    globalcontrolflag = 1;

    if(strcmp(buf,globaldirectory.c_str())!=0)
    {
    globaldirectory = buf;
    globalcontrolflag=2;
    }

    }

    //Create new saving directory if necessary

    if(globalcontrolflag==2)
    {
    outputFile.close();
    Data_saving_path = globaldirectory.c_str();
    mkdir(Data_saving_path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); 
    chdir(Data_saving_path);
    mkdir("raw", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); 
    mkdir("rect", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); 
    mkdir("disparity", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    outputFile.open("timestamp.txt");
    //Now that we created and entered the path, continue to save images
    globalcontrolflag==1;
    }


    //Create file names
    if(globalcontrolflag==1)
    {
    //Raw left and right images
    sprintf(ssl,"./raw/%06dl.jpg",count);
    sprintf(ssr,"./raw/%06dr.jpg",count);
    //rectified left and right images
    sprintf(ssrl,"./rect/%06drl.jpg",count);
    sprintf(ssrr,"./rect/%06drr.jpg",count);
    //disparity map
    sprintf(ssd,"./disparity/%06dd.jpg",count);
     }



    //Get BBimage
  if ( stereoCamera.bColor )
   {
  
      // get the images from the capture buffer and do all required processing
      // note: produces a TriclopsInput that can be used for stereo processing

      extractImagesColor( &stereoCamera,
			  DC1394_BAYER_METHOD_NEAREST,
			  pucDeInterlacedBuffer,
			  pucRGBBuffer,
			  pucGreenBuffer,
			  &pucRightRGB,
			  &pucLeftRGB,
			  &pucCenterRGB,
			  &input );
   

    IplImage* tmp =cvCreateImage(cvSize(stereoCamera.nCols, stereoCamera.nRows),IPL_DEPTH_8U,3);
    IplImage* left_image =cvCreateImage(cvSize(stereoCamera.nCols, stereoCamera.nRows),IPL_DEPTH_8U,3);
    IplImage* right_image =cvCreateImage(cvSize(stereoCamera.nCols, stereoCamera.nRows),IPL_DEPTH_8U,3);

    memcpy ( tmp->imageData, (char*)pucRightRGB, 3*stereoCamera.nCols*stereoCamera.nRows );
    cvCvtColor(tmp, right_image, CV_RGB2BGR);

    //Save RAW images 
    if((globalcontrolflag==1)||(globalcontrolflag==2))
    {
    //Raw right
    cvSaveImage(ssr, right_image);

    outputFile << ssr << endl;
    gettimeofday(&now, NULL);
    sprintf(currenttime,"%u.%06u", now.tv_sec, now.tv_usec);
    outputFile << currenttime << endl;


    memcpy ( tmp->imageData, (char*)pucLeftRGB, 3*stereoCamera.nCols*stereoCamera.nRows );


    cvCvtColor(tmp, left_image, CV_RGB2BGR);
    //Raw left
    cvSaveImage(ssl, left_image);

    outputFile << ssl << endl;
    gettimeofday(&now, NULL);
    sprintf(currenttime,"%u.%06u", now.tv_sec, now.tv_usec);
    outputFile << currenttime << endl;


    }



  //  if(IMG_SHOW_WIN_FLAG){
  //  cvNamedWindow( "Raw", CV_WINDOW_AUTOSIZE );
  //  cvShowImage("Raw", right_image);
  //  cvWaitKey(2);

  //  }


   //Release memory
    cvReleaseImage(&right_image );
    cvReleaseImage(&left_image );
    cvReleaseImage(&tmp );
 }



   // Make sure we are in subpixel mode
   triclopsSetSubpixelInterpolation( triclops, 1 );

   e = triclopsRectify( triclops, &input );

   if ( e != TriclopsErrorOk )
   {
      fprintf( stderr, "triclopsRectify failed!\n" );
      triclopsDestroyContext( triclops );
      cleanup_and_exit( camera );
      return 1;
   }


   // Preparing rectified images

   //rectified image size
   int SGBM_nCols=240;
   int SGBM_nRows=320;

   IplImage* leftRecImage;
   IplImage* rightRecImage;
   //create Images
   rightRecImage = cvCreateImage (cvSize(SGBM_nCols,SGBM_nRows),IPL_DEPTH_8U,1);
   leftRecImage = cvCreateImage (cvSize(SGBM_nCols,SGBM_nRows),IPL_DEPTH_8U,1);
   
   //get and save the rectified images
   TriclopsImage image;

   triclopsGetImage( triclops, TriImg_RECTIFIED, TriCam_LEFT, &image );
   //memory copy L
   memcpy(leftRecImage->imageData,image.data,leftRecImage->height*leftRecImage->widthStep);

   triclopsGetImage( triclops, TriImg_RECTIFIED, TriCam_RIGHT, &image );
   //memory copy R
   memcpy(rightRecImage->imageData,image.data,rightRecImage->height*rightRecImage->widthStep);
   Mat imgl,imgr;
   imgr = Mat(SGBM_nCols,SGBM_nRows, CV_8U, rightRecImage->imageData);
   imgl = Mat(SGBM_nCols,SGBM_nRows, CV_8U, leftRecImage->imageData);

   //Save Rectified images 
   if(globalcontrolflag==1){
   imwrite(ssrr, imgr);

    outputFile << ssrr << endl;
    gettimeofday(&now, NULL);
    sprintf(currenttime,"%u.%06u", now.tv_sec, now.tv_usec);
    outputFile << currenttime << endl;

   imwrite(ssrl, imgl);
    outputFile << ssrl << endl;
    gettimeofday(&now, NULL);
    sprintf(currenttime,"%u.%06u", now.tv_sec, now.tv_usec);
    outputFile << currenttime << endl;
    }


   //Release memory
   cvReleaseImage(&rightRecImage);
   cvReleaseImage(&leftRecImage);

    count++;
   //ros::spinOnce();

  }
   /////////////////////////Main Loop Ends Here///////////////////////////

   //Close filestream
   outputFile.close();
   //Close UDP socket
   close(sockfd);

   //  Stop data transmission
   printf( "Stop transmission\n" );
   if ( dc1394_video_set_transmission( stereoCamera.camera, DC1394_OFF ) != DC1394_SUCCESS )
   {
      fprintf( stderr, "Couldn't stop the camera?\n" );
   }

   //Delete buffer
   delete[] pucDeInterlacedBuffer;
   if ( pucRGBBuffer )
      delete[] pucRGBBuffer;
   if ( pucGreenBuffer )
      delete[] pucGreenBuffer;


   // close camera
   cleanup_and_exit( camera );


  
  return 0;
}


